import {Injectable } from '@angular/core';
import { LogService } from './log.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject } from 'rxjs/';
import { map } from 'rxjs/operators';

@Injectable()
export class StarWarsService {
  private characters = [
    { name : 'hamza bndr', side : ''},
    { name : 'hamza1 bndr1', side : ''}
  ];

  private logService: LogService;
  characterChanged = new Subject<void>();
  httpClient: HttpClient;

  constructor(logServie: LogService, httpClient: HttpClient) {
    this.logService = logServie;
    this.httpClient = httpClient;
  }

  fetchCharacters() {
    // this.httpClient.post('https://swapi.co/api/people',data)

    this.httpClient.get('http://swapi.co/api/people')
    .subscribe(
      (data) => {
       // console.log(data);
        const extractedChars = data['results'];
        const chars = extractedChars.map((char) => {
          return {name: char.name, side : ''};
        });
        console.log(chars);
        this.characters = chars;
        this.characterChanged.next();

      }
    );
  }

  getCharacters(chosenList) {
    if (chosenList === 'all') {
      return this.characters.slice();
    }
    return this.characters.filter((char) => {
      return char.side === chosenList;
    });
  }

  onSideChosen(charInfo) {
    const pos = this.characters.findIndex(char => {
      return char.name === charInfo.name;
    });
    this.characters[pos].side = charInfo.side;
    this.characterChanged.next();
    this.logService.writeLog('changed size of ' + charInfo.name);
  }

  addCharacter(name, side) {
    const pos = this.characters.findIndex(char => {
      return char.name === name;
    });
    if (pos !== -1) {
      return;
    }
    const newChar = {name, side};
    this.characters.push(newChar);
  }
}


