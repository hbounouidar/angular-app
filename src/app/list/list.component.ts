import { Component, OnInit, Input, Output, EventEmitter, OnDestroy  } from '@angular/core';
import { StarWarsService } from '../star-wars.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit, OnDestroy {
  // @Input() characters;
  characters = [];
  appRoutingModule: ActivatedRoute;
  swService: StarWarsService;
  loadedSide = 'all';
  subscription: Subscription;
  // @Output() sideAssigned = new EventEmitter<{name: string, side: string}>();
  constructor(appRoutingModule: ActivatedRoute, swService: StarWarsService) {
    this.appRoutingModule = appRoutingModule;
    this.swService = swService;
  }

  ngOnInit() {

    this.appRoutingModule.params.subscribe(
      (params) => {
        this.characters = this.swService.getCharacters(params.side);
        this.loadedSide = params.side;
      }
    );
    this.subscription = this.swService.characterChanged.subscribe(
      () => {
        this.characters = this.swService.getCharacters(this.loadedSide);
      }
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  /*onSideAssigned(charInfo) {
    this.sideAssigned.emit(charInfo);
  }*/

}
